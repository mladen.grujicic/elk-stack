# Elk stack 

using __elasticsearch__ version **7.6.1**
    
# How it works?

![Image](/images/image.png)
    
# How to run using minikube?

**minikube** start --vm-driver=hyperkit --memory=8192 --cpus=2

**kubectl** apply -f elk-stack.yaml

**kubectl** apply -f elasticsearch.yaml

**kubectl** apply -f logstash.yaml
    
**kubectl** apply -f filebeat.yaml
    
**kubectl** apply -f kibana.yaml
    
# Enable kibana
Because only one elasticsearch node is used, we need to set number of replicas from default 1 to 0.

    PUT /$INDEX/_settings
    {
        "index" : {
            "number_of_replicas" : 0
        }
    }




